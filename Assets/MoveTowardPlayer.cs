﻿using UnityEngine;
using System.Collections;

public class MoveTowardPlayer : MonoBehaviour {
	public float moveEvery =2f;
	public float force = 50;
	float countDownMove=2f;
	void Update () {
		countDownMove-=Time.deltaTime;
		if (countDownMove < 0) {
			countDownMove=moveEvery;	
			GameObject player = GameObject.FindWithTag("Player") as GameObject;
			Vector3 direction = (player.transform.position-transform.position);
			direction.Normalize();
			rigidbody2D.AddForce(direction*force);
		}
	}
}
