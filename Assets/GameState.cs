﻿using UnityEngine;
using System.Collections;

public class GameState : MonoBehaviour {

	public static GameState singleton;
	public LevelState levelState;
	public Flubber creeperState;

	private float steveProgNeedToWin=0.95f;
	public float creeperMinDistToWin=0.07f;

	public GameObject winMenu;
	public GameObject loseMenu;
	public GameObject aboutMenu;

	public bool menuAlreadyDisplay;

	void Start()
	{


		if (singleton == null)
						singleton = this;
		if (levelState == null) {
			Debug.LogWarning("The game can't be define withoult level state",this.gameObject);
			Destroy(this);
		}
		if(winMenu!=null)
		winMenu.SetActive (false);
		if(loseMenu!=null)
		loseMenu.SetActive (false);
		if(aboutMenu!=null)
		aboutMenu.SetActive (false);
	}

	void Update () {
	
		bool steveWin = DoesSteveIsArriveToTheDiamond ();
		if (steveWin)
						DisplayCreeperDeathMenu ();
		if (creeperState != null) {
			if(creeperState.GetStability()==Flubber.StabilityState.Dislocated)
			{
				GameEvent.KillCreeperWithoutExplosion();
			}		
		}
	
	}

	public static bool DoesSteveIsArriveToTheDiamond ()
	{
		if (singleton == null || singleton.levelState == null)
						return false;
		float steveProg = singleton.levelState.GetSteveProgressionInPct ();
		return steveProg >singleton.steveProgNeedToWin;
	}

	
	public  bool IsCreeperNearEnought ()
	{
		float steveProg =levelState.GetSteveProgressionInPct ();
		float creeperProg =levelState.GetCreeperProgressionInPct ();
		return IsCreeperNearEnought (steveProg, creeperProg);
	}
	
	private  bool IsCreeperNearEnought (float steveProg, float creeperProg)
	{
		Debug.Log("S:"+((int)(steveProg*100f))+"  C:"+((int)(creeperProg*100f))+" Pct:"+ (steveProg-creeperMinDistToWin));
		return creeperProg > steveProg - creeperMinDistToWin && creeperProg < steveProg + creeperMinDistToWin;
	}

	public bool IsCreeperNearEnoughtWithVisualInformation ()
	{
		float steveProg =levelState.GetSteveProgressionInPct ();
		float creeperProg =levelState.GetCreeperProgressionInPct ();
		return IsCreeperNearEnought (steveProg, creeperProg);
	}
	
	public static void DisplayCreeperDeathMenu ()
	{
		if (singleton!=null && singleton.menuAlreadyDisplay)
			return;singleton.menuAlreadyDisplay = true;
		if (singleton!=null && singleton.loseMenu != null)
			singleton.loseMenu.SetActive (true);
		
	}
	public static void DisplaySteveDeathMenu ()
	{
		if (singleton!=null && singleton.menuAlreadyDisplay)
			return;singleton.menuAlreadyDisplay = true;
		if (singleton!=null && singleton.winMenu != null)
			singleton.winMenu.SetActive (true);
		
	}

	public static void DisplayAboutInformation ()
	{
		if(singleton.winMenu!=null)
			singleton.winMenu.SetActive (false);
		if(singleton.loseMenu!=null)
			singleton.loseMenu.SetActive (false);
		if (singleton!=null && singleton.aboutMenu != null)
			singleton.aboutMenu.SetActive (true);
	}

	public static void NextLevel()
	{
		int level = PlayerPrefs.GetInt ("Level");
		PlayerPrefs.SetInt ("Level",++level);
	}

	public static void SetLevelTo (int lvl)
	{
		PlayerPrefs.SetInt ("Level",lvl);
	}

	public static int GetLevel()
	{
		return PlayerPrefs.GetInt ("Level");
	}
}
