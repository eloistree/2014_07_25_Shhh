﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * PrIMD42@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this code. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 *  S. E.
 * Donate a beer: http://www.primd.be/donate/ 
 * Contact: http://www.primd.be/
 * ----------------------------------------------------------------------------
 */

using UnityEngine;
using System.Collections.Generic;
using System;
/**Manage the access to sound of the game that are instanciated by GameSound, SceneSound class*/
public class Sound {

	public static Dictionary< string , List<AudioClip> > sounds = new Dictionary< string , List<AudioClip>  > ();
	public static Dictionary <string, int> soundsNotFound = new Dictionary<string ,int >();
	public enum SoundPickUpType  {First, Last, Randomly, Next}
	public static  Vector3 defaultPosition = Vector3.zero;
	public static string defaultStorageName = "#SoundStorageZone";
	public static  Transform soundStorage;
	public static bool lookInResourcesAllow = true;


	/**If not transform is given when play,
	 * the sound is created at the default position in vector term.
	 * SetPosition define this default position*/
	public static  void SetPosition(Vector3 position){
		defaultPosition = position;
	}
	public static void SetStorageZoneForSound (string name){
		if (name == null || name.Length < 1)
						name = defaultStorageName;
		GameObject soundStore = GameObject.Find (name);
		if(soundStore==null)
				soundStore = new GameObject (name);
		SetStorageZoneForSound (soundStore.transform);
	}
	public static void SetStorageZoneForSound (Transform localisation){
		soundStorage = localisation;
	}

	public static void ResetSoundNotFound(){
		soundsNotFound.Clear ();
		}

	public static GameObject Play (string name, float volume, int priority=100)
	{
		return Play ( name, null,true,SoundPickUpType.Randomly,10,volume,0,priority);
	}

	public static GameObject Play(string name, Transform positionPt = null, bool withFollow=true ,SoundPickUpType pickUpType = SoundPickUpType.Randomly , float lifeTime= 10f, float volume=80f, ulong withDelay=0, int priotity=100)
	{
		//USED VARIABLE DECLARATION
		GameObject sound =null;
		AudioClip originalSound=null;
		AudioSource source = null;

		//DO WE HAVE THE SOUND
		originalSound= GetSound(name, pickUpType);
		if (originalSound == null)
						return null;
		//OK SO CREATE THE SOUND GAME OBJECT
		sound = new GameObject (name+ " Sound: "+ originalSound.name );
		source = sound.AddComponent<AudioSource> () as AudioSource;
		source.clip = originalSound;




		//NOW POSITIONATE THE SOUND
		if(positionPt!=null){
			sound.transform.position = positionPt.transform.position;
			if(withFollow) sound.transform.parent=positionPt;
		}
		else if(positionPt==null)
		{
			sound.transform.position = defaultPosition;
			//A BIT OF STORAGE CAN BE USEFULL
			if (soundStorage != null)
				sound.transform.parent = soundStorage;
		}

		if (sound != null) {
		
			source.volume= ((float)volume)/100f;
			source.priority= priotity;
		}
		//DOES A LIFE TIME IS REQUIRED
		if (sound != null && lifeTime > 0) {
			GameObject.Destroy(sound, lifeTime);
		}



		//WE ARE READY ? OK LET PLAY THE SOUND OF OUR PEOPLE
		if (originalSound != null) {
			if(withDelay<=0) source.Play ();
			else source.Play(withDelay);	
		}

		//RETURN THE SOUND GAME OBJECT TO LET THE USER DO WHAT EVER HE WANT
		return sound;
	}

	public static AudioClip GetSound(string name, SoundPickUpType pickUpType)
	{
		AudioClip sound=null;
		//DOES WE HAVE THE SOUND
		sound= GetSoundStored (name, pickUpType);
		
		//WE DO NOT ! OK, WE WILL TRY TO CREATE IT BASED ON RESOURCE
		if (sound == null && lookInResourcesAllow ) {
			sound = GetResourcesAudio (name);

			if(sound !=null)
			{//OH YEAH, WE CAN ADD IT. BECAUSE IT WILL PROBABLY BE REUSED
				AddSound(name, sound);

				Debug.LogWarning("The sound "+sound+" for the keyword "+name+" has been reach by using Find in Resources." +
				                 "Maybe, it could be add directly to sound in aim to have a O(1) complexity");
			}
			else 
			{
				if( ! soundsNotFound.ContainsKey(name)){
					soundsNotFound.Add(name, 1);
					Debug.LogWarning("The sound associate to the keyword "+name+" has not been found.");
				}
			}
		}
		return sound;
	}

	public static AudioClip GetSoundStored( string name , SoundPickUpType pickUpType )
	{
		List<AudioClip> soundPossible=null;
		AudioClip [] soundPossibleArray = null;;
		////GET LIST
		try{
			sounds.TryGetValue(name,out soundPossible );
		}catch(Exception){return null;}
		if (soundPossible == null || soundPossible.Count<=0)
						return null;

		//// CONVERT TO ARRAY
		soundPossibleArray = soundPossible.ToArray ();


		///RETURN USER WANTED SOUND
		switch (pickUpType) {
			case SoundPickUpType.First: return soundPossibleArray[0];
			case SoundPickUpType.Last: return soundPossibleArray[soundPossibleArray.Length-1];
			case SoundPickUpType.Randomly: return soundPossibleArray[UnityEngine.Random.Range(0,soundPossibleArray.Length) ];
		} 
		return null;
	}
	public static AudioClip GetResourcesAudio(  string name )
	{
		if (name == null || name.Length < 1)
						return null;
		AudioClip audio = Resources.Load <AudioClip>(name) as AudioClip;
		if(audio==null)
			audio = Resources.Load <AudioClip>("sound/"+name) as AudioClip;
		if(audio==null)
			audio = Resources.Load <AudioClip>("Sound/"+name) as AudioClip;
		if(audio==null)
			audio = Resources.Load <AudioClip>("sounds/"+name) as AudioClip;
		if(audio==null)
			audio = Resources.Load <AudioClip>("Sounds/"+name) as AudioClip;
		//AudioClip [] allAudioInResources =  Resources.FindObjectsOfTypeAll<AudioClip>() as AudioClip [];
		
//		Debug.Log ("Look resources: "+allAudioInResources.Length);
//		foreach (AudioClip a in allAudioInResources) {
//			if(a!=null && a.name!=null && a.name.Contains(name))
//			{
//				return a;
//			}
//
//		}


		return audio;

	}



	public static void  AddSound(string name, GameObject sound){

		//CHECK PARAMS
		if (sound == null || name == null || name.Length<=0)
						return;
		//IS IT REALLY A SOUND
		AudioClip audio = GetAudio (sound);
		if (audio == null)
						return ;
		AddSound (name, audio);

		}


	public static void AddSound(string name,AudioClip audio)
	{
		//CHECK PARAMS
		if (audio == null || name == null || name.Length <= 0)
						return;
		//GET LIST SOUND OF NAME
		List<AudioClip> list = null;
		sounds.TryGetValue (name, out list);
		if(list!=null)
		{
			//YEAH A SEVERAL SOUND LIST :)
			if( ! list.Contains(audio))
			list.Add(audio);
			//Debug.Log("New Sound Stored: "+name);
		}
		else if(list==null)
		{
			//NEW SOUND !
			List <AudioClip> newList = new List<AudioClip>();
			newList.Add(audio);
			sounds.Add(name, newList);
			//Debug.Log("New Sound Available: "+name);
		}

	}

	public static string [] GetSoundStoredNames()
	{
		string [] names = new string[sounds.Count];
		sounds.Keys.CopyTo(names, 0);
		return names;
	}
	public static AudioClip[] GetSoundsForName(string name)
	{
		List<AudioClip> list = null;
		sounds.TryGetValue (name, out list);
		if(list==null) return null;
		return list.ToArray ();
	}

	public static AudioClip GetAudio(GameObject soundGamo)
	{
		if (soundGamo == null)
						return null;
		AudioClip audio =null;

		//// TRY TO GET AUDIO FROM THE OBJECT
		AudioSource source = soundGamo.GetComponent<AudioSource>() as AudioSource;
		if (source != null)
			audio = source.clip;

		//IF NOT, TRY TO GET AUDIO FROM THE CHILDREN OBJECT
		if(audio==null)
			{
			source = soundGamo.GetComponentInChildren<AudioSource>() as AudioSource;
			if (source != null)
				audio = source.clip;
				//audio = soundGamo.GetCompoen
			}
		return audio;
	}

	public static string [] GetSoundNotFound ()
	{
		string [] notFound = new string[soundsNotFound.Keys.Count];
		soundsNotFound.Keys.CopyTo (notFound,0);
		return notFound;

	}
}
