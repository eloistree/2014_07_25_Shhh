﻿using UnityEngine;
using System.Collections;


	public class SpecialBonusLevel : MonoBehaviour {
		
		public GameObject bossPrefab;
		public int everyXNiveau=3;
		public float timeToPop=3f;
		public float whenStart=0f;
		// Use this for initialization
		void Start () {
			if (bossPrefab == null)
				Destroy (this);
			
			int level = GameState.GetLevel ();
			if (level % everyXNiveau != 0)
				Destroy (this);
			whenStart = Time.time;
		}
		
		// Update is called once per frame
		void Update () {
			if (Time.time - whenStart > timeToPop) {
				NitroFactory.CreateNitro(Random.Range(0f,1f),-0.01f,bossPrefab);
			}
			
		}
	}