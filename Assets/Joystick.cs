﻿using UnityEngine;
using System.Collections;

public class Joystick : MonoBehaviour {

	public Flubber flubber;
	public Texture2D downSprite;
	public Texture2D upSprite;

	public float timeToBePression=0.7f;
	
	private bool isPressing;
	private float whenPressionDetected;

	private float whenStartPression;
	private Vector3 whereStartPression;

	private float whenIsPression;
	private Vector3 whereIsPression;
	
	public float downJoySize=50f;
	public float upJoySize=40f;

	void OnGUI()
	{
		if (flubber == null || downSprite == null || upSprite == null)
						return;

		MouseObserver ();

		if (isPressing) {

			Vector2 direction = new Vector2(whereIsPression.x -whereStartPression.x,whereIsPression.y -whereStartPression.y);
		


			float limDistance= downJoySize/4f;
			float xStartPress = whereStartPression.x-limDistance;
			float yStartPress = Screen.height-whereStartPression.y-limDistance;
			float xWherePress = xStartPress+direction.normalized.x*limDistance;
			float yWherePress = yStartPress-direction.normalized.y*limDistance;
			if(direction.x<limDistance && direction.x>-limDistance) xWherePress =xStartPress+direction.x;
			if(direction.y<limDistance && direction.y>-limDistance) yWherePress =yStartPress-direction.y; 
	
			GUI.DrawTexture(new Rect(xStartPress,yStartPress, downJoySize,downJoySize),downSprite);
			GUI.DrawTexture(new Rect(xWherePress,yWherePress,upJoySize,upJoySize),upSprite);

			flubber.SetDirection(direction.normalized.x,direction.normalized.y);
		}


	}

	void MouseObserver ()
	{
		if (Input.GetMouseButtonDown (0)) {
			whenPressionDetected = Time.time;
		}
		if (Input.GetMouseButton (0) && Time.time - whenPressionDetected > timeToBePression) {
			if (!isPressing) {
				isPressing = true;
				whenStartPression = Time.time;
				whereStartPression = Input.mousePosition;
			}
			whenIsPression = Time.time;
			whereIsPression = Input.mousePosition;
		}
		if (Input.GetMouseButtonUp (0)) {
			ResetPressingInfo ();
		}
	}

	void ResetPressingInfo ()
	{
		isPressing=false;
		whenPressionDetected=0f;

		whenStartPression=0f;
		whereStartPression=Vector3.zero;
		
		whenIsPression=0f;
		whereIsPression=Vector3.zero;

		if (flubber != null)
						flubber.SetDirection (0, 0);
	}
}
