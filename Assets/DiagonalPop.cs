﻿using UnityEngine;
using System.Collections;

public class DiagonalPop : MonoBehaviour {

	
	public float defaultSpeed=-0.03f;
	private float countDownWave;
	private int stepCount;
	public float timeBetweenWave=0.5f;
	private bool leftToRight=true;
	public int [] whichLevel;
	
	void Start()
	{
		int level = GameState.GetLevel ();
		
		bool isPresent = false;
		foreach (int lvl in whichLevel) {
			if(lvl==level){isPresent=true;break;}
		}
		if(!isPresent)Destroy (this);
		
		countDownWave = timeBetweenWave/2f;
		defaultSpeed -= level * 0.001f;
		
	}
	public void Update(){
		
		countDownWave -= Time.deltaTime;
		if (countDownWave < 0f) {
			countDownWave=timeBetweenWave;
			NitroFactory.CreateNitro(0.1f*(float)stepCount,defaultSpeed);
	
		if (leftToRight)
			stepCount++;
				else
			stepCount--;
			if (stepCount <= 0)
			leftToRight = true;
			if (stepCount >= 10)
			leftToRight = false;

		}
			
	}
}
