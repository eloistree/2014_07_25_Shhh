﻿using UnityEngine;
using System.Collections;
using System;
public class NitroFactory : MonoBehaviour {

	private static NitroFactory singleton;

	public GameObject nitroModele;
	public Transform leftPoint;
	public Transform rightPoint;
	public Transform topPoint;
	public float nitroGravity;

	void Start(){
		if (singleton == null)
						singleton = this;
				else
						Debug.LogWarning ("There are two NitroFactory when they should be only one", this.gameObject);


	}
	
	private float countDown;


	
	public static void CreateNitro(float pourcentFromLeft)
	{
		if (singleton == null) {
			Debug.LogWarning("A nitro should be created but the nitro factory singleton is not define");
						return ;
				}
		CreateNitro (pourcentFromLeft, singleton.nitroGravity,singleton.nitroModele);
	}
	
	public static void CreateNitro(float pourcentFromLeft, float nitroGravity){
		CreateNitro (pourcentFromLeft, nitroGravity, singleton.nitroModele);
	}
	public static void CreateNitro(float pourcentFromLeft, float nitroGravity, GameObject prefab)
	{
		if (singleton == null) {
			Debug.LogWarning("A nitro should be created but the nitro factory singleton is not define");
			return ;
		}
		else if (singleton != null && (singleton.nitroModele == null || singleton.leftPoint==null|| singleton.rightPoint==null|| singleton.topPoint==null ) ) {
			Debug.LogWarning("A nitro should be created but the nitro factory singleton is not define correctly");
			return ;
		}

		pourcentFromLeft = Mathf.Clamp (pourcentFromLeft, 0f, 1f);
		float startHeight = (singleton.leftPoint.position.y + singleton.rightPoint.position.y) / 2f;
		float whereOnWidth = singleton.leftPoint.position.x + (singleton.rightPoint.position.x - singleton.leftPoint.position.x) * pourcentFromLeft;
		Vector3 position = new Vector3 (whereOnWidth,startHeight,0f);

		GameObject nitroObject = Instantiate (prefab, position, singleton.nitroModele.transform.rotation)as GameObject;
		Nitro nitro = nitroObject.AddComponent<Nitro> () as Nitro;
		if (nitro != null) {
			nitro.SetGravity(nitroGravity);
			nitro.SetMaxHeight(singleton.topPoint.position.y);
		}
		
	}


}
