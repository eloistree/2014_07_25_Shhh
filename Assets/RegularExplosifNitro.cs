﻿
	using UnityEngine;
	using System.Collections;
	
	public class RegularExplosifNitro : MonoBehaviour {
		private float countDown;
		public float timeBetween=3f;
	public GameObject explosifNitro;
		
		void Start()
		{
		timeBetween += Random.Range (-1f, 3f);
		}
		public void Update(){
			
			countDown -= Time.deltaTime;
			if (countDown < 0f) {
			NitroFactory.CreateNitro(UnityEngine.Random.Range(0f,1f), -0.01f,explosifNitro);	
				countDown=timeBetween;
			}
			
		}
	}
