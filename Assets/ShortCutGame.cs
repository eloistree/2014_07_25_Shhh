﻿using UnityEngine;
using System.Collections;

public class ShortCutGame : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.R) || Input.GetKeyDown("enter") || Input.GetKeyDown("return")) {
			Application.LoadLevel(Application.loadedLevel);	
		}
		
		if (Input.GetKeyDown (KeyCode.Escape) || Input.GetKeyDown(KeyCode.Delete)) {
			Application.Quit();
		}
	}
}
