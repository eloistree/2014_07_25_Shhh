﻿using UnityEngine;
using System.Collections;

public abstract class NitroBehaviour : MonoBehaviour {
	
	public float timeStart;
	public float timeEnd;

	public float [] popTimes;


	/**cursor is incremented*/
	public abstract bool Next (float timeNow);
	/**return when is the next one*/
	public abstract float WhenNext();
	/**return -1f(left) ->  0f (center) -> 1f (right)*/
	public abstract float whereNext();

}
