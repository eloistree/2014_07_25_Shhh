﻿using UnityEngine;
using System.Collections;

public class LinearPop : MonoBehaviour {

	public float defaultSpeed=-0.02f;
	private float countDown;
	public float timeBetweenWave=5f;
	public int [] whichLevel;
	
	void Start()
	{
		int level = GameState.GetLevel ();

		bool isPresent = false;
		foreach (int lvl in whichLevel) {
			if(lvl==level){isPresent=true;break;}
		}
		if(!isPresent)Destroy (this);

		countDown = timeBetweenWave/2f;
		defaultSpeed -= level * 0.003f;

	}
	public void Update(){
		
		countDown -= Time.deltaTime;
		if (countDown < 0f) {
			int ignore = UnityEngine.Random.Range(0,3);
			if(ignore==0)
			{
				NitroFactory.CreateNitro(0.4f,defaultSpeed);
				NitroFactory.CreateNitro(0.5f,defaultSpeed);
				NitroFactory.CreateNitro(0.6f,defaultSpeed);
				NitroFactory.CreateNitro(0.7f,defaultSpeed);
				NitroFactory.CreateNitro(0.8f,defaultSpeed);
				NitroFactory.CreateNitro(0.9f,defaultSpeed);
				NitroFactory.CreateNitro(0.10f,defaultSpeed);	

			}
			else 
				if(ignore==1)
			{
				NitroFactory.CreateNitro(0.0f,defaultSpeed);
				NitroFactory.CreateNitro(0.1f,defaultSpeed);
				NitroFactory.CreateNitro(0.2f,defaultSpeed);
				NitroFactory.CreateNitro(0.8f,defaultSpeed);
				NitroFactory.CreateNitro(0.9f,defaultSpeed);
				NitroFactory.CreateNitro(0.10f,defaultSpeed);	
				
			}

			
			else 
				if(ignore==2)
			{
				NitroFactory.CreateNitro(0.0f,defaultSpeed);
				NitroFactory.CreateNitro(0.1f,defaultSpeed);
				NitroFactory.CreateNitro(0.2f,defaultSpeed);
				NitroFactory.CreateNitro(0.3f,defaultSpeed);
				NitroFactory.CreateNitro(0.4f,defaultSpeed);
				NitroFactory.CreateNitro(0.5f,defaultSpeed);
				NitroFactory.CreateNitro(0.6f,defaultSpeed);	
				
			}
			countDown=timeBetweenWave;
		}
		
	}
}
