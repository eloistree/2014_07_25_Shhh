﻿using UnityEngine;
using System.Collections;

public class TNT_Trigger : MonoBehaviour {

	private bool isInContactWithFlubber;
	
	private void OnTriggerEnter2D(Collider2D col)
	{
		
		if (col.gameObject.CompareTag ("Player"))
			isInContactWithFlubber = true;
	}
	private void OnTriggerExit2D(Collider2D col)
	{
		if (col.gameObject.CompareTag ("Player"))
						isInContactWithFlubber = false;
	}

	public bool IsActive (){return isInContactWithFlubber;}
}
