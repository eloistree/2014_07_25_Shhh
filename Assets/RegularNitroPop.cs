﻿using UnityEngine;
using System.Collections;

public class RegularNitroPop : MonoBehaviour {
	private float countDown;
	public float timeBetween=1f;

	void Start()
	{
		int level = GameState.GetLevel ();
		if (level < 4)
			timeBetween -= level * 0.3f;
		else if (level < 10)
			timeBetween -= 0.4f+level * 0.01f;
		else if (level < 30)
			timeBetween -= level * 0.02f;
		else
			timeBetween -= level * 0.01f;
	}
	public void Update(){
		
		countDown -= Time.deltaTime;
		if (countDown < 0f) {
			NitroFactory.CreateNitro(UnityEngine.Random.Range(0f,1f));	
			countDown=timeBetween;
		}
		
	}
}
