﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class ExplosionForce : MonoBehaviour {


	public Vector2 direction;

	public float rotationFrom=-180f;
	public float rotationTo=180f;
	public float explosionPowerForce=2;

	// Use this for initialization
	void Start () {
		rigidbody2D.AddTorque (Random .Range(rotationFrom, rotationTo));
		rigidbody2D.AddForce (direction * explosionPowerForce);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
