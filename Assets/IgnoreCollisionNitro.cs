﻿using UnityEngine;
using System.Collections;

public class IgnoreCollisionNitro : MonoBehaviour {
	public string wallOnlyPlayerLayerName;
	public string nitroLayerName;
	// Use this for initialization
	void Start () {
		Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer (nitroLayerName), LayerMask.NameToLayer (wallOnlyPlayerLayerName));
	}
	

}
