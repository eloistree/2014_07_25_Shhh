﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Flubber : MonoBehaviour {

	public float mass = 1f;
	public float massLostPerSecond =0.05f;
	public float massCriticalLostPerSecond =0.4f;
	public float instability=1.2f;
	
	public float criticalMass=1.1f;
	public float perfectMass=0.95f;
	public float nearDislocation=0.3f;
	public float dislocation=0.2f;
	
	public float upMoveSpeed=1f;
	public float downMoveSpeed=0.5f;
	public float leftMoveSpeed=0.8f;
	public float rightMoveSpeed=0.8f;

	public float initialGravity;
	public Vector3 initialScale;
	public Vector3 initialPosition;



	public float maxSize=15f;
	private float actualSize=1f;
	private float realSize;


	public float progressionByNitro=0.01f;
	public float massWinByExplosifNitro = 1f;
	//private float velocitySizeChange=0.1f;

	public float massWinByNitro = 0.3f;

	public bool isDeath;
	
	private Vector2 direction;
	public void SetDirection (Vector2 dir){direction = dir;}
	public void SetDirection (float x,float y){direction.x = x;direction.y = y;}
	void Start () {
		initialScale = transform.localScale;
		initialPosition = transform.position;
		initialGravity = transform.rigidbody2D.gravityScale;
	}


	// Update is called once per frame
	void Update () {
	
		GravityAndControl ();
		SizeUnderTimeControl ();

	
	}

	void GravityAndControl ()
	{
		transform.rigidbody2D.gravityScale = initialGravity;
		float horizontal = Input.GetAxis ("Horizontal");
		float vertical = Input.GetAxis ("Vertical");
		if (direction.x != 0)
			horizontal = Mathf.Clamp(direction.x,-1f,1f);
		if (direction.y != 0)
			vertical = Mathf.Clamp(direction.y,-1f,1f);
		
		float stateSpeedEffect=1f;
		StabilityState state = GetStability ();
		if (state == StabilityState.Instable) stateSpeedEffect = 0.3f;
		else if (state == StabilityState.Normal) stateSpeedEffect = 1.5f;
		else if (state == StabilityState.NearDislocation) stateSpeedEffect = 3.0f;
		else if (state == StabilityState.Dislocated) stateSpeedEffect = 0.1f;
		float speedRation = Time.deltaTime * 100f *stateSpeedEffect;

		if (horizontal < 0) {
			transform.rigidbody2D.AddForce (new Vector2 (horizontal * leftMoveSpeed*speedRation, 0f));
		}
		else
		if (horizontal > 0) {
			transform.rigidbody2D.AddForce (new Vector2 (horizontal * rightMoveSpeed*speedRation, 0f));
		}
		if (vertical < 0) {
			transform.rigidbody2D.AddForce (new Vector2 (0f, vertical * downMoveSpeed*speedRation));
		}
		else
		if (vertical > 0) {
			transform.rigidbody2D.AddForce (new Vector2 (0f, vertical * upMoveSpeed*speedRation));
		}
	}

	void SizeUnderTimeControl ()
	{
		float lost = massLostPerSecond;
		StabilityState state = GetStability ();
		if (state == StabilityState.NearDislocation || state == StabilityState.CriticalMass) {
			lost = massCriticalLostPerSecond;
		}
		if (mass > 0)
			mass -= lost * Time.deltaTime;
		else
			mass = 0;
		float size = Mathf.Clamp (mass - dislocation, 0f, maxSize);
		if (state == StabilityState.CriticalMass||state == StabilityState.Instable)
						size = Mathf.Pow (size, 3);
		realSize = size;
		actualSize = Mathf.Lerp (actualSize, realSize, Time.deltaTime);
		actualSize = Mathf.Clamp (actualSize, 0f, maxSize);
		transform.localScale = new Vector3 (actualSize, actualSize, actualSize/3f);
	}


	private float lastTimeEatNitro ;
	public float minTimeBetweenToBeFull;
	public void OnCollisionEnter2D(Collision2D col)
	{
		if (isDeath)return;
		if (col.gameObject.CompareTag ("Nitro") ||col.gameObject.CompareTag ("NitroExplosif") ) {
			if(this.gameObject.activeInHierarchy){
			mass+=massWinByNitro;
				bool full = Time.timeSinceLevelLoad-lastTimeEatNitro>minTimeBetweenToBeFull;
				lastTimeEatNitro=Time.timeSinceLevelLoad;

				if (col.gameObject.CompareTag ("Nitro") )
					LevelState.AddSpeedUpAtCreeper(full?progressionByNitro:progressionByNitro/5f);
				if (col.gameObject.CompareTag ("NitroExplosif") )
					mass+=massWinByExplosifNitro;
				Sound.Play("Absorb");
			}
		}
		
		
	}

	public void SetAsDeath (bool b)
	{
		isDeath = true;
	}


	public StabilityState GetStability(){
		if (mass < dislocation) return StabilityState.Dislocated;
		else if (mass < nearDislocation) return StabilityState.NearDislocation;
		else if (mass < perfectMass) return StabilityState.Normal;
		else if (mass < criticalMass) return StabilityState.PerfectMass;
		else if (mass < instability) return StabilityState.CriticalMass;
		return StabilityState.Instable;
	}
	public enum StabilityState {Dislocated, NearDislocation, Normal, PerfectMass, CriticalMass,Instable}



}
