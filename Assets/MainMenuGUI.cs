﻿using UnityEngine;
using System.Collections;

public class MainMenuGUI : MonoBehaviour {

	public Texture2D keepCalm;
	public Rect keepCalmPosition;
	public GUIStyle playStyleButton;
	public Rect playButtonPosition;


	void OnGUI()
	{
		float x = Screen.width / 2f - keepCalmPosition.width / 2f + keepCalmPosition.x;
		float y =  Screen.height/20f +  keepCalmPosition.y;
		GUI.DrawTexture (new Rect (x, y, keepCalmPosition.width, keepCalmPosition.height),keepCalm);
		
		x = Screen.width / 2f - playButtonPosition.width / 2f + playButtonPosition.x;
		y +=keepCalmPosition.height + playButtonPosition.y;
		if (GUI.Button (new Rect (x, y, playButtonPosition.width, playButtonPosition.height), "", playStyleButton)) {
			Application.LoadLevel (1);
		}
	}
}
