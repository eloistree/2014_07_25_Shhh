﻿using UnityEngine;
using System.Collections;

public class LevelState : MonoBehaviour {

	public static LevelState singleton;
	
	public float stevePctPerSecond=0.01f;
	public float stevePctProgression=0.5f;
	public float creeperPctPerSecond= 0.03f;
	public float creeperPctProgression=0.0f;

	public Transform startPoint;
	public Transform endPoint;

	public Transform steve;
	public Transform creeper;


	// Use this for initialization
	void Start () {
		if (singleton == null)
			singleton = this;
		else
			Debug.LogWarning ("There are two LevelState when they should be only one", this.gameObject);

	}
	
	public static void AddSpeedUpAtSteve(float pct){
		if (singleton == null)
						return;
		singleton.stevePctProgression += pct;
		singleton.stevePctProgression = Mathf.Clamp (singleton.stevePctProgression, 0f, 0.96f);
	}
	public static void AddSpeedUpAtCreeper(float pct){
		if (singleton == null)
			return;
		float behindSteve = singleton.stevePctProgression - 0.03f;
		singleton.creeperPctProgression += pct;
		if (singleton.creeperPctProgression > behindSteve)
						singleton.creeperPctProgression = behindSteve;
		
	}



	// Update is called once per frame
	void Update () {
		
		AddSpeedUpAtSteve(stevePctPerSecond * Time.deltaTime);
		AddSpeedUpAtCreeper(creeperPctPerSecond * Time.deltaTime);

		LocateOnTheMap (steve,stevePctProgression);
		LocateOnTheMap (creeper,creeperPctProgression);
	}

	public  void LocateOnTheMap(Transform prefabToLocate, float pourcentFromLeft)
	{
		if (prefabToLocate == null) {
			Debug.LogWarning("The prefab given should note be null",this.gameObject);
			return ;
		}

		Vector3 position = GetLocalisationOf (pourcentFromLeft);
		position.z = prefabToLocate.position.z;
		prefabToLocate.position = Vector3.Lerp (prefabToLocate.position,position, Time.deltaTime*3f);
		
	}
	
	public float GetSteveProgressionInPct ()
	{
		return stevePctProgression;
	}
	
	public float GetCreeperProgressionInPct ()
	{
		return creeperPctProgression;
	}

	public Vector3 GetLocalisationOf(float pourcentFromLeft)
	{
		pourcentFromLeft = Mathf.Clamp (pourcentFromLeft, 0f, 1f);
		float height = (startPoint.position.y + endPoint.position.y) / 2f;
		float whereOnWidth = startPoint.position.x + (endPoint.position.x - startPoint.position.x) * pourcentFromLeft;
		float z = (startPoint.position.z + endPoint.position.z) / 2f;
		return  new Vector3 (whereOnWidth,height,z);

	}
}
