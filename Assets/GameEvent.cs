﻿using UnityEngine;
using System.Collections;

public class GameEvent : MonoBehaviour {

	public static GameEvent singleton;
	public GameState gameState;
	public LevelState levelState;
	
	public GameObject atomicPrefab;
	public GameObject basicExplosionPrefab;
	public GameObject steveInPiecesPrefab;
	public Transform creeper;
	public Transform steve;

	 void Start(){
		if (gameState == null) {
			Debug.LogWarning("GameState have to be define to send some event");
			Destroy(this);
			return;
		}
		if (singleton == null)
						singleton = this;

	}

	static void CreateCreeperExplosion (bool isNearEnought)
	{
		if (isNearEnought)
			Sound.Play ("BoumWin");
		else
			Sound.Play ("BoumLose");
		if (singleton.creeper == null ||singleton.steve == null || singleton.basicExplosionPrefab==null || singleton.atomicPrefab==null)
						return;

		Vector3 explosionPosition = singleton.creeper.position;
		if(singleton.levelState!=null)explosionPosition= singleton.levelState.GetLocalisationOf(singleton.levelState.GetCreeperProgressionInPct ());
		if (isNearEnought)
			GameObject.Instantiate (singleton.atomicPrefab, explosionPosition, singleton.atomicPrefab.transform.rotation);
		else
			GameObject.Instantiate (singleton.basicExplosionPrefab, explosionPosition, singleton.basicExplosionPrefab.transform.rotation);

	}

	static void KillCreeper ()
	{
		singleton.creeper.gameObject.SetActive(false);
		Destroy (singleton.creeper.gameObject.rigidbody2D);
		Destroy (singleton.creeper.gameObject.collider2D);
	}

	static void KillSteve ()
	{
		singleton.steve.gameObject.SetActive (false);
		GameObject.Instantiate (singleton.steveInPiecesPrefab, singleton.steve.transform.position, singleton.steve.transform.rotation);
	}
	public static void KillCreeperWithoutExplosion()
	{
		KillCreeper ();
		GameState.DisplayCreeperDeathMenu ();
	}
	public static void MakeCreeperExplode ()
	{
		if (singleton==null || singleton.gameState==null)
						return;

		bool isNearEnought = singleton.gameState.IsCreeperNearEnoughtWithVisualInformation ();
		CreateCreeperExplosion (isNearEnought);

		GameEvent.KillCreeper ();
		if (isNearEnought) {
			bool isSteveAlreadyArrive = GameState.DoesSteveIsArriveToTheDiamond();
						GameEvent.KillSteve ();
					if(!isSteveAlreadyArrive){
						GameState.NextLevel ();
						GameState.DisplaySteveDeathMenu ();
					}
				} else {
			
			GameState.DisplayCreeperDeathMenu();		
		}
	

	}
	public static void SendSteveWinEvent ()
	{
		throw new System.NotImplementedException ();
	}


}
