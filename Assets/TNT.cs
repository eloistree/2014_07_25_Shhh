﻿using UnityEngine;
using System.Collections;

public class TNT : MonoBehaviour {

	public Flubber creeper;
	public TNT_Trigger left;
	public TNT_Trigger right;


	public bool explosionPlayed ;
	public float timeBetween=1f;


	public bool hasExplosed;
	private float whenStartExplosion;

	void Start(){
		if (left == null || right == null)
						Destroy (this);
	}

	void Update () {
	
		if (!hasExplosed && left.IsActive () && right.IsActive ()) {
			Explose();
		}

		if (hasExplosed && !explosionPlayed &&( Time.time > (whenStartExplosion + timeBetween))) {
			explosionPlayed =true;
			GameEvent.MakeCreeperExplode();

		}
	}

	void Explose ()
	{
		hasExplosed = true;
		whenStartExplosion = Time.time;
		Sound.Play ("Shhh");
		if (creeper != null)
						creeper.SetAsDeath (true);
		//send event of explosion
	}


}
