﻿using UnityEngine;
using System.Collections;

public class Nitro : MonoBehaviour {

	private float maxHeight=100000f;
	private bool isAutoDestructing;
	private Transform followUntilDeath;

	
	// Update is called once per frame
	void Update () {
	
		if (!isAutoDestructing && transform.position.y > maxHeight) {
			AutoDestruction();		
		}
		if (isAutoDestructing) {
			float size = transform.localScale.y;
			size-=size*0.05f;
			size = Mathf.Clamp(size,0f,2f);
			transform.localScale=new Vector3(size,size,size);

			
			if(followUntilDeath!=null)
				transform.position = Vector3.Lerp(transform.position,followUntilDeath.position,Time.deltaTime);
		}
	}

	public void SetGravity (float nitroGravity)
	{
		if (transform.rigidbody2D == null)
						return;
		transform.rigidbody2D.gravityScale = nitroGravity;
	}

	public void SetMaxHeight (float height)
	{
		maxHeight = height;
	}

	void AutoDestruction ()
	{
		isAutoDestructing = true;
		Destroy (this.gameObject);
	}
	void AutoDestruction (bool withEffect)
	{
		isAutoDestructing = true;
		Destroy(GetComponent<Collider2D>());
		Destroy (this.gameObject,2f);
	}


	public void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.CompareTag ("Player")) {
			AutoDestruction (true);
			followUntilDeath = col.transform;
		
		}


	}
}
