﻿using UnityEngine;
using System.Collections;

public class AboutButton : MonoBehaviour {

	void OnMouseDown()
	{
		GameState.DisplayAboutInformation ();
	}
}
