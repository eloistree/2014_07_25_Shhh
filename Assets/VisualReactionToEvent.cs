﻿using UnityEngine;
using System.Collections;

public class VisualReactionToEvent : MonoBehaviour {

	
	public GameObject creeperIngame;
	public GameObject steveIngame;
	public GameObject atomicPrefab;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	
	public void KillCreeper(){}
	public void MakeDisappearCreeper()
	{

	}
	public void CreateAtomicExplosion(bool epic){}
	public void PlayEpicExplosionSound(){}
	public void PlayLoseExplosionSound(){}
	public void PlayShhhhSound(){}
}
