﻿using UnityEngine;
using System.Collections;

public class LevelTextUpdate : MonoBehaviour {

	public TextMesh levelText;
	// Use this for initialization
	void Start () {
		levelText.text = "Level "+GameState.GetLevel ();
	}

}
